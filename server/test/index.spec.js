const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const {expect} = require('chai');
const { connectDB, disconnectDB } = require('../db/index');
chai.should();
chai.use(chaiHttp);

describe('todos_api_test', async () => {
    beforeEach(async () => {
        try {
          await connectDB();
        } catch (err) {
          console.log(err);
        }
    });
   
    afterEach(async () => {
        try {
          await disconnectDB();
        } catch (err) {
          console.log(err);
        }
    });

    const todoExample = {
        "text": "buy some milk",
    }

    it('todo list must be an empty array',  async () => {
        // todo listesi boş bir dizi olmalı
        var response = await chai.request(server).get('/api/todo');
        console.log(response.body)
        expect(response.body.length.should.eql(0));
    })

    it('should create a todo',  async () => {
        //
        var response = await chai.request(server).post('/api/todo').send(todoExample);
        expect(response.should.have.status(200))
        delete response.body._id;
        delete response.body.__v;
        expect(response.body.should.eql(todoExample));
    })
   
    it('should fetch all the todos', async() => {
        const response = await chai.request(server).get('/api/todo')
        expect(response.should.have.status(200));
    })
});