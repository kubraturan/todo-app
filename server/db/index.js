const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

const uri = process.env.MONGO_URI || "mongodb://localhost:27017/?directConnection=true&serverSelectionTimeoutMS=2000";

const dbOptions = {
  useNewUrlParser: true,
};

const connectDB = async () => {
  // Mocking Database if environment is 'test'
  if (process.env.NODE_ENV === 'test') {
    try {
      const mockDb = await MongoMemoryServer.create();
      const mockURI = await mockDb.getUri();

      console.error(mockURI)
      await mongoose.connect(mockURI);
      console.log("Database connected")
    } 
    catch (err) {
      console.error(err.message);
    }
  } 
  else {
    try {
        console.log(uri)
        await mongoose.connect(uri, dbOptions);
            
        console.log('Database connected');
    } 
    catch (err) {
      console.error(err.message);
      process.exit(1);
    }
  }
};

const disconnectDB = async () => {
  try {
    await mongoose.disconnect();
  } 
  catch (err) {
    console.error(err.message);
  }
};

module.exports = { connectDB, disconnectDB };