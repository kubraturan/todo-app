const Todo = require('../models/todo');

class TodoController {
    async postTodo(req, res) {
        const { text } = req.body;

        try {
            const newTodo = new Todo({text});

            const todo = await newTodo.save();

            return res.status(200).json(todo);
        } 
        catch (err) {
            console.error(err);
            return res.status(500).send('Internal Server Error');
        }
    }

    async getTodo(req, res) {
        try {
            const todoList = await Todo.find();
            todoList.length > 0
              ? res.status(200).json(todoList)
              : res.status(200).send(new Array());
        } 
        catch (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
        }
    }
}

module.exports = new TodoController();