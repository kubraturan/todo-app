const express = require('express');
const cors = require('cors');
const { connectDB } = require('./db/index');

const todoRouter = require('./routes/todo');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 5000;

connectDB();

app.use(express.json({ extended: false }));
app.use(cors());

app.use('/api/todo', todoRouter);

app.listen(PORT, () => console.log(`Server started on ${PORT}`));

module.exports = app;
