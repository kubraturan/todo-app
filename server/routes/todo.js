const router = require('express').Router();
const controller = require('../controllers/todo');
const { check } = require('express-validator');

router.post("/", [check('text', 'Todo text is required.').not().isEmpty()], controller.postTodo);
router.get("/", controller.getTodo);

module.exports = router;