const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  id: {
    type: Number,
    autoIncrement: true,
    primaryKey: true
  },
  text: {
    type: String
  }
});

module.exports = Todo = mongoose.model('todo', TodoSchema);