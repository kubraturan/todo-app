# Todo App
✨ The app was created using many new technologies.✨

![gif](./client/public/app.gif)

## Technologies
- React
- Node.js
- MongoDBAtlas
- Express
- Docker
- Caddy
- Jest & Enzyme & Puppeteer
- Mocha & Chai

## API Architecture
API run :5000/api/todo

Node.js, express and chai are used for testing.

The Router requests are:

- GET: Gets All of the todo items list.
- POST: Adds a todo item.

Integrated Mongo Models with API, so all of the states are stored in database.

## Dockerization

- Dockerize react client [Dockerfile](./client/Dockerfile.dev)
- Dockerize api server [Dockerfile](./server/Dockerfile)
- Development for set up docker compose [docker-compose.yml](./docker-compose-dev.yml)
- Production for set up docker compose [docker-compose.yml](./docker-compose-production.yml)

Using Caddy web server for web server due to its easy setup, cloudflaure support and reverse proxy capability.

# Testing & Tools
#### Client Side

Jest & Anzyme & Puppeteer: Used for testing client

Usage:

```sh
cd client/

yarn run test:unit

yarn run test:integration

yarn run test acceptance
```
#### Server Side

Mocha & Chai: Used for testing API endpoints

Usage:

```sh
cd server/

yarn run test
```

# How does the project work in general terms? 
The most common use of Makefiles was to manage the dependencies of the source files of the programs during the compilation and linking (build) phase, that is, to compile only the files that need to be compiled by looking at the dependencies on each other and the last modified dates of the source files when compiling the programs.

> `Note` : Sets MONGO_URI based on information from env
 
> `Note` : For security reasons, we ignored the local.env and production.env files in the config file under the server folder.

- [Makefile](./Makefile) with commands

####  Development Mode:

```sh
make build-dev
make run-dev
```

####  Production Mode
✅ Set up SSL (using Caddy)

✅ Create Digital Ocean VM

✅ Create HTTP/HTTPS/SSH firewall rule and attach to VM

✅ Whitelist IP address of Server in Atlas

You can access it here [Todo App](https://list.to-do.win)

**It was a great experience for me, I learned a lot of innovations in a short time and my enthusiasm and desire to learn new things increased. Thanks for this nice experience.**
