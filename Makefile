### DEV
SSH_STRING:=root@157.245.46.30

build-dev:
	cd client && $(MAKE) build-dev
	cd server && $(MAKE) build

run-dev:
	ssh $(SSH_STRING) "docker stack deploy --compose-file docker-compose-dev.yml react-app-dev"

### PROD

build-production:
	cd client && $(MAKE) build-production
	cd server && $(MAKE) build	

run-production:
	ssh $(SSH_STRING) "docker stack deploy --compose-file docker-compose-production.yml react-app-production"
	
stop:
	docker-compose down

# apt install make

ssh:
	ssh $(SSH_STRING)

copy-files:
	scp -r ./* $(SSH_STRING):/root/

# when you add firewall rule, have to add SSH on port 22 or it will stop working

# run challenge with cloudflare on flexible, then bump to full

