import React from 'react';

const Todo = ({ todos }) => {
  return (
    <div id="todo-list" className="todo-list">
      {
        todos.length > 0 &&
        todos.map((todo, index) => (
          <div
            className={todo.isComplete ? 'todo-row complete' : 'todo-row'}
            key={index}
          >
            <div key={todo.id}>
              {todo.text}
            </div>
          </div>
        ))
      }
    </div>
  )
};

export default Todo;
