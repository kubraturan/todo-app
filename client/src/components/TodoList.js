import React, { useState, useEffect } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';
import TodoService from '../api/index';

function TodoList() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    getTodoList();
  }, [])

  const addTodo = todo => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }

    try {
      return (
        TodoService.add(todo)
        .then(getTodoList)
        .catch(err => console.log(err))
      )
    }
    catch(err) {
      console.log(err.message)
    }
  };

  const getTodoList = () => {
    try {
      return TodoService.getTodoList()
      .then(setTodos)
      .catch(err => console.log(err))
    }
    catch (err) {
      console.log(err.message);
    }
  }

  return (
    <>
      <h1>What Todo Next?</h1>
      <TodoForm onSubmit={addTodo} />
      <Todo
        todos={todos}
      />
    </>
  );
}

export default TodoList;
