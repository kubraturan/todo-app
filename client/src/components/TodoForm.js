import React, { useState, useEffect, useRef } from 'react';

function TodoForm(props) {
  const [value, setValue] = useState('');

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const handleChange = e => {
    setValue(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();

    props.onSubmit({
      //id: Math.floor(Math.random() * 10000),
      text: value
    });
    setValue('');
  };

  return (
    <form onSubmit={handleSubmit} className='todo-form'>
      <input
        id="add-todo"
        type="text"
        placeholder='Add a todo'
        value={value}
        onChange={handleChange}
        name='text'
        className='todo-input'
        ref={inputRef}
      />
      <button onClick={handleSubmit} className='todo-button' id="add-todo-button">
        Add todo
      </button>
    </form>
  );
}

export default TodoForm;
