export const URL = process.env.REACT_APP_BASE_URL;

class TodoService {
    /**
     * Written method to get all todo list.
    */
    async getTodoList() {
        return (
            await fetch(`${URL}/todo`)
            .then(res => res.json())
        )
    }

    /**
     * method used to add todo.
    */
    async add(todo) {
        return (
            fetch(`${URL}/todo`, {
                method: 'POST',
                body: JSON.stringify(todo),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            .then(res => res.json())
        )
    }
}

export default TodoService = new TodoService();
