import puppeteer from 'puppeteer';
import assert from 'assert';

jest.setTimeout(50 * 1000);

test('Acceptance test', async () => {
  const browser = await puppeteer.launch({
    //headless: false,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage'
    ]
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080
  });

  // Given Empty ToDo list
  // When I write "buy some milk" to <text-box> and
  // Then I should see "buy some milk" item in ToDo
  await page.goto("https://list.to-do.win");
  await page.waitForTimeout(5000);
  await page.focus("input[id=add-todo]");
  await page.type("input[id=add-todo]", "buy some milk");
  await page.click("button[id=add-todo-button]");
  const todos = await page.$eval(".todo-list", div => div.innerText);
  await assert.ok(todos.includes("buy some milk"))

  browser.close();
});
