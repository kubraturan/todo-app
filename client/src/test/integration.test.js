import React from "react";
import {mount} from "enzyme";
import { TodoForm, Todo } from "../components";

function setup() {
    var todoProps = {
        todos: new Array()
    }

    var todoFormProps = {
        onSubmit: jest.fn()
    }
    const todoFormWrapper = mount(<TodoForm {...todoFormProps}/>)
    const todoWrapper = mount(<Todo {...todoProps} />)

    return {
        todoFormWrapper,
        todoWrapper
    }
}

describe("Todo integration testing", () => {
    test("Is todos an empty array?", () => {
        const {todoWrapper} = setup();

        expect(todoWrapper.props().todos).toEqual(new Array());
    });

    test("Focus on input, write buy some milk", async () => {
        const {todoFormWrapper} = setup();
        const input = todoFormWrapper.find('#add-todo');
        expect(input.simulate('focus'));

        expect(input.simulate("change", { target: { value: "Buy some milk" }}))
    });

    test("Add the todo by clicking the button or pressing enter", () => {
        const {todoFormWrapper} = setup();
        const button = todoFormWrapper.find('#add-todo-button');
        expect(button.simulate('click'));

        expect(todoFormWrapper.find('#add-todo').simulate('keypress', {key: 'Enter'}))

        // clickde ekleme işlemi kaldı bir tek
    });

    test("Empty input after adding todo", () => {
        const {todoFormWrapper} = setup();
        const input = todoFormWrapper.find('#add-todo');

        expect(input.simulate("change", { target: { value: "" }}))
    });
});