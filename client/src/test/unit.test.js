import React from "react";
import { mount} from "enzyme";
import { TodoList, Todo, TodoForm } from "../components";

describe("Todo component testing", () => {
    var wrapper = null;

    function setup() {
        const componentProps = {
          todos: new Array(),
        };
    
        const props = {
          todos: new Array(),
        };
       
       const todoWrapper = mount(<Todo {...props} />)
       
        return {
          componentProps,
          todoWrapper
        }
    }

    beforeEach(() => {
        wrapper = mount(<TodoList />);
    });

    test("do you have a todos prop?", () => {
        const { todoWrapper } = setup();
        expect(todoWrapper.props().todos);
    });

    test("wrapper props and componentProps equal ? ", () => {
        const { todoWrapper, componentProps } = setup();
        expect(todoWrapper.props().todos).toEqual(componentProps.todos);
    });
}); 
 
describe("TodoForm component testing", () => {
    let wrapper = null;
    beforeEach(() => {
        wrapper = mount(<TodoForm />);
    });

    test("Is there an input? Is it type text? Is the id add-todo ?", () => {
        const input = wrapper.find('input');
        expect(input)
        expect(input.props().type).toEqual('text');
        expect(input.props().id).toEqual('add-todo');
    });

    test("Is there an button? Is the id add-todo-button?", () => {
        const button = wrapper.find('button');
        expect(button)
        expect(button.props().id).toEqual('add-todo-button');
    });
});
 
describe("TodoList component testing", () => {
    let wrapper = null;
    beforeEach(() => {
      wrapper = mount(<TodoList />);
    });
  
    test("h1 tag content is What Todo Next?", () => {
      expect(wrapper.find("h1").text()).toContain("What Todo Next?");
    });
  
    test("Is there a TodoForm component?", () => {
      expect(wrapper.find('TodoForm').exists()).toBe(true)
    });
  
    test("Is there a Todo component? ? ", () => {
      expect(wrapper.find('Todo').exists()).toBe(true)
    });
});